import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';

// Apollo
import { GraphQLModule } from './graphql.module';


import { SubscriptionOrder } from './services/subscription-order.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,

    GraphQLModule,
  ],
  providers: [
    SubscriptionOrder,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
