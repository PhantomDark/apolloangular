import { Injectable } from '@angular/core';
import { Subscription } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root',
})
export class SubscriptionOrder extends Subscription {
    document = gql`
        subscription subscriptionOrder {
            orders {
                id
                client
                description
                status
            }
        }
    `;
}
