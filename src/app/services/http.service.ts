// import { Injectable, EventEmitter } from '@angular/core';
// import { Response, RequestMethod } from '@angular/http';
// import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpRequest } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
// import { Subject } from 'rxjs/Subject';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/concatMap';
// import 'rxjs/add/operator/takeUntil';
// import { ApiMessageModel } from '../models/api-message.model';

// @Injectable()
// export class HttpService {

//     public ngUnsubscribe: Subject<void> = new Subject<void>();

//     constructor(
//         private http: HttpClient,
//     ) {
//     }

//     public error: EventEmitter<ApiMessageModel> = new EventEmitter<ApiMessageModel>();

//     public get<TResult>(templateUrl: string, params: any, headers: any = undefined): Observable<TResult> {
//         return this.request<TResult>(templateUrl, params, RequestMethod.Get, undefined, headers);
//     }

//     public post<TResult>(templateUrl: string, params: any, body: any, headers: any = undefined): Observable<TResult> {
//         return this.request<TResult>(templateUrl, params, RequestMethod.Post, body, headers);
//     }

//     public put<TResult>(templateUrl: string, params: any, body: any, headers: any = undefined): Observable<TResult> {
//         return this.request<TResult>(templateUrl, params, RequestMethod.Put, body, headers);
//     }

//     public delete<TResult>(templateUrl: string, params: any, body: any, headers: any = undefined): Observable<TResult> {
//         return this.request<TResult>(templateUrl, params, RequestMethod.Delete, body, headers);
//     }

//     public request<TResult>(templateUrl: string, params: any, method: RequestMethod, body: any, objHeaders: any): Observable<TResult> {
//         const result: Subject<TResult> = new Subject<TResult>();

//         let searchParams: HttpParams = new HttpParams();

//         for (const paramProp in params) {
//             if (params.hasOwnProperty(paramProp)) {
//                 const value: any = params[paramProp];
//                 if (value !== null && value !== undefined) {
//                     if (Array.isArray(value)) {
//                         value.forEach(element => {
//                             searchParams = searchParams.append(paramProp, element);
//                         });
//                     } else {
//                         searchParams = searchParams.append(paramProp, value);
//                     }
//                 }
//             }
//         }

//         const url = templateUrl.replace(/:[a-zA-z]{1}[a-zA-z0-9]+/, (match: string, args: any[]) => {
//             const paramName: string = match.substr(1);
//             const value = searchParams.get(paramName);
//             if (!value) { // url params are important!
//                 throw new URIError('parameter ' + match + ' in url \'' + url + '\' is not defined.');
//             }
//             searchParams.delete(paramName); // don't send duplicated parameters.
//             return value;
//         });

//         let headers = new HttpHeaders();

//         for (const headerProp in objHeaders) {
//             if (objHeaders.hasOwnProperty(headerProp)) {
//                 headers = headers.append(headerProp, objHeaders[headerProp]);
//             }
//         }

//         this.http.request(RequestMethod[method], url, {
//             body: body,
//             params: searchParams,
//             headers: headers
//         })
//         .takeUntil(this.ngUnsubscribe)
//         .catch(this._mapException.bind(this))
//         .subscribe(data => {
//             result.next(<TResult>data);
//             result.complete();
//         },
//         error => {
//             result.error(error);
//             result.complete();
//         });

//         return result.asObservable();
//     }

//     protected _mapResult<TResult>(response: Response): TResult {
//         try {
//             return <TResult>response.json();
//         } catch (e) {
//             return <TResult>null;
//         }
//     }

//     protected _mapException(response: Response, observable: Observable<any>) {
//         let apiMessage: ApiMessageModel = new ApiMessageModel();
//         if (response.status === 0) {
//             apiMessage = new ApiMessageModel();
//             apiMessage.code = -1;
//             apiMessage.message = 'Please check your internet connection.';
//             apiMessage.title = 'Error connecting to server';
//             apiMessage.type = 'Error';

//         } else {
//             try {
//                 const jsonResponse = response.json();
//                 apiMessage.code = response.status;
//                 apiMessage.message = jsonResponse.Message || jsonResponse.Text || jsonResponse.error_description;
//                 apiMessage.title = jsonResponse.Title || 'Error';
//                 apiMessage.type = 'Error';
//             } catch (err) {
//                 if (response.status === 403) {
//                     apiMessage.code = response.status;
//                     apiMessage.message = 'Your session has finished. Please Login again!';
//                     apiMessage.title = 'Error';
//                     apiMessage.type = 'Error';
//                 }
//             }
//         }

//         if (apiMessage.code !== 405 && apiMessage.code !== 400 && apiMessage.code !== 403) {
//             this.error.emit(apiMessage);
//         }

//         throw apiMessage;
//     }

//     protected _urlJoin(urlA: string, urlB: string) {
//         if (urlA.endsWith('/')) {
//             urlA = urlA.substr(0, urlA.length - 1);
//         }

//         if (!urlB.startsWith('/')) {
//             urlB = '/' + urlB;
//         }

//         return urlA + urlB;
//     }
// }
// export { RequestMethod } from '@angular/http';
