// src/app/graphql.module.ts

import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { HttpHeaders } from '@angular/common/http';

// Apollo
import { ApolloModule, Apollo, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';

import { split } from 'apollo-link';

import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

@NgModule({
    exports: [
        HttpClientModule,
        ApolloModule,
        HttpLinkModule
    ],
    // providers: [
    //     {
    //         provide: APOLLO_OPTIONS,

    //     }
    // ]
})
export class GraphQLModule {
    constructor(apollo: Apollo, private httpClient: HttpClient) {
        // apollo.create({
        //   link: httpLink.create({ uri: 'https://realm-test-rc.us1a.cloud.realm.io/graphql' }),
        //   cache: new InMemoryCache()
        // });

        // const link = onError(({ graphQLErrors, networkError }) => {
        //     if (graphQLErrors) {
        //       graphQLErrors.map(({ message, locations, path }) =>
        //         console.log(
        //           `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
        //         ),
        //       );
        //     }

        //     if (networkError) {
        //         console.log(`[Network error]: ${networkError}`);
        //     }
        //   });


        // https://realm-test-rc.us1a.cloud.realm.io/graphql/default
        // const token = 'eyJhcHBfaWQiOiJpby5yZWFsbS5BdXRoIiwiaWRlbnRpdHkiOiJhMTZjMjkwOGE1NzMxYzZhMjFkMDg0ZWYzMGUwOTA3YyIsImFjY2VzcyI6WyJyZWZyZXNoIl0sInNhbHQiOiJjYzVmYjk5ZCIsImV4cGlyZXMiOjE4NjUwNzc3NTcsImlzX2FkbWluIjp0cnVlLCJpc0VtYWlsQ29uZmlybWVkIjpmYWxzZX0=:kEf34OIgR+/B/P22RB79FU9gEI9xYGTqoSxnkiohyu+2IdYXKwlr8SBD9Ccu5IV3zdl37KdU5e/X5ijMVCn6tBO5Gr6bIGLtKKUrMYtHShJCOi2rNJIz5Uwcayupg3RRnF8r99wqhn69AIgCm7ZadvVXCd/CXsfvzd+588R7Wl950CfFwhIvTn5bHZkR6r9BX0AH/1ClJQFCu0zjNInTg1SiPBorbPt6usVFAlTZ4OR5GArwNmWUZ4ZpWvFYifJVE0Sf8xjsXav4HwQf+4+xUe/iqVllqrAL2GjvfdfWyz78gWrYv+Xk4KjqKo5QGrtUbfdQXyegYtY8dnnCpt4UQw==';
        const token = 'eyJhcHBfaWQiOiJpby5yZWFsbS5BdXRoIiwiaWRlbnRpdHkiOiJhMTZjMjkwOGE1NzMxYzZhMjFkMDg0ZWYzMGUwOTA3YyIsImFjY2VzcyI6WyJyZWZyZXNoIl0sInNhbHQiOiI3MGMxNmE0NCIsImV4cGlyZXMiOjE4NjUwODY0NzQsImlzX2FkbWluIjp0cnVlLCJpc0VtYWlsQ29uZmlybWVkIjpmYWxzZX0=:EsWiI3gv5iV0EcsFqV+/Leml3O2xjt10WeVjLAqsXiFS0rdFSfbaokBYB2GndyazWMRIA/eFZTKd1BihvRrb8gwK1DbRBmNcjJWzOcQWlNgqhUItYIzQF0FpJGMn77B6kfGvjkNyCqvHtgziRUYf+lFO+duS6zoDXZEn8UfCGQO9CpaDTgFTkzNITUMNslEmZxHLAZXgj3eAKie97SemM6SjJeDJ+Q81O71nVcz28es7Vq8qh8kDi83r3NKBLqig2oqHy0iw6jjA0ECHtVtrkDg+vxmBtChaGMiLg1fiawrPVTyLCkrVnAVqaB7nnodtp+2d+zD8KcNd8pEr0fFQrA==';
        const httpLink = new HttpLink(httpClient).create({
            uri: 'https://testing-realm-database.us1a.cloud.realm.io/graphql/default'
        });

        const subscriptionLink = new WebSocketLink({
            uri:
                'wss://testing-realm-database.us1a.cloud.realm.io/graphql/default',
            options: {
                reconnect: true,
                connectionParams: {
                    token: localStorage.getItem('token') || token
                }
            }
        });

        const link = split(
            ({ query }) => {
                const { kind, operation } = getMainDefinition(query);
                return kind === 'OperationDefinition' && operation === 'subscription';
            },
            subscriptionLink,
            httpLink
        );

        apollo.create({
            link,
            cache: new InMemoryCache()
        });

        ///////////////

        // const http = httpLink.create({ uri: 'https://testing-realm-database.us1a.cloud.realm.io/graphql/default' });

        // const auth = setContext((_, { headers }) => {
        //     // get the authentication token from local storage if it exists
        //     //   const token = localStorage.getItem('token');
        //     const token = 'eyJhcHBfaWQiOiJpby5yZWFsbS5BdXRoIiwiaWRlbnRpdHkiOiJhMTZjMjkwOGE1NzMxYzZhMjFkMDg0ZWYzMGUwOTA3YyIsImFjY2VzcyI6WyJyZWZyZXNoIl0sInNhbHQiOiJjYzVmYjk5ZCIsImV4cGlyZXMiOjE4NjUwNzc3NTcsImlzX2FkbWluIjp0cnVlLCJpc0VtYWlsQ29uZmlybWVkIjpmYWxzZX0=:kEf34OIgR+/B/P22RB79FU9gEI9xYGTqoSxnkiohyu+2IdYXKwlr8SBD9Ccu5IV3zdl37KdU5e/X5ijMVCn6tBO5Gr6bIGLtKKUrMYtHShJCOi2rNJIz5Uwcayupg3RRnF8r99wqhn69AIgCm7ZadvVXCd/CXsfvzd+588R7Wl950CfFwhIvTn5bHZkR6r9BX0AH/1ClJQFCu0zjNInTg1SiPBorbPt6usVFAlTZ4OR5GArwNmWUZ4ZpWvFYifJVE0Sf8xjsXav4HwQf+4+xUe/iqVllqrAL2GjvfdfWyz78gWrYv+Xk4KjqKo5QGrtUbfdQXyegYtY8dnnCpt4UQw==';
        //     // return the headers to the context so httpLink can read them
        //     // in this example we assume headers property exists
        //     // and it is an instance of HttpHeaders
        //     if (!token) {
        //         return {};
        //     } else {
        //         // return {
        //         //   headers: headers.append('Authorization', `${token}`)
        //         // };
        //         return { headers: { Authorization: `${token}` } };
        //     }
        // });

        // apollo.create({
        //     link: auth.concat(http),
        //     cache: new InMemoryCache()
        //     // other options like cache
        // });
    }
}
