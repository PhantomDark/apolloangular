import { Component, OnInit, OnDestroy } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SubscriptionOrder } from './services/subscription-order.service';
import { Subscription } from 'rxjs';

const allOrdersQuery = gql`
  query allOrders {
    orders {
      id
      client
      description
      status
    }
  }
`;

const subscriptionOrder = gql`
  subscription subscriptionOrder {
    orders {
        id
        client
        description
        status
    }
  }
`;

const getOrderByStatusQuery = gql`
  query getByStatus($query: String) {
    orders(query: $query) {
      id
      client
      description
      status
    }
  }
`;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'angularApollo';
  orders: any[];

  id: number;
  client: string;
  description: string;
  status: number;

  ordersSubscription: Subscription;

  todoQuery: QueryRef<any>;

  constructor(private apollo: Apollo, private orderWatcher: SubscriptionOrder) { }

  ngOnInit() {
    // this.apollo.watchQuery<any>({
    //   errorPolicy: 'all',
    //   query: gql`
    //     query allOrders {
    //       orders {
    //         id
    //         client
    //         description
    //         status
    //       }
    //     }
    //   `,
    // })
    //   .valueChanges
    //   .subscribe(
    //     res => {
    //       console.log('get all orders', res);
    //       this.orders = res.data.orders;
    //     },
    //     error => console.log('jajo1', error)
    //   );

    this.todoQuery = this.apollo.watchQuery({
      query: allOrdersQuery
    });

    this.ordersSubscription = this.todoQuery.valueChanges.subscribe(
      ({ data }) => {
        console.log('all', data);
        this.orders = data.orders;
        // this.todoItems = [...data.allTodos];
      }
    );

    this.setupSubscription();

    // this.orderWatcher.subscribe(data => console.log('order changes', data));

    // this.ordersSubscription = this.apollo
    //   .subscribe({
    //     query: subscriptionOrder
    //   })
    //   .subscribe(({ data }) => {
    //     console.log('order changes 2', data);
    //   });
  }

  setupSubscription() {
    this.todoQuery.subscribeToMore({
      document: subscriptionOrder,
      updateQuery: (prev, { subscriptionData }) => {
        console.log('more', subscriptionData);

        if (!subscriptionData.data) {
          this.orders = prev;
          return prev;
        }

        // const newTodo = subscriptionData.data.Todo.node;

        // return Object.assign({}, prev, {
        //   allTodos: [...prev['allTodos'], newTodo]
        // });

        this.orders = subscriptionData.data.orders;
      }
    });
  }

  addOrder() {
    console.log(this.id, this.client, this.description, this.status);
    // const addCar = gql`
    //     mutation addCarMutation(
    //       $make: String!
    //       $model: String!
    //       $miles: Int!
    //     ) {
    //       addCar(
    //         make: $make
    //         model: $model
    //         miles: $miles
    //       ) {
    //         make
    //         model
    //         miles
    //       }
    //     }
    //   `;

    const addOrder = gql`
      mutation addingOrder($id: Int, $client: String, $description: String, $status: String) {
        addOrder(input: {id: $id, client: $client, description: $description, status: $status } ) {
          id
          client
          description
          status
        }
      }
      `;

    this.apollo
      .mutate({
        mutation: addOrder,
        variables: {
          id: this.id,
          client: this.client,
          description: this.description,
          status: this.status
        }
      })
      .subscribe(
        ({ data }) => {
          console.log("got order added", data);
          this.orders.push(data.addOrder);
          console.log('orders', this.orders);
        },
        error => {
          console.log("there was an error sending the query", error);
        }
      );
  }

  // addCar2() {
  //   console.log(this.id, this.model, this.miles);

  //   const addCar = gql`
  //     mutation addingCar2($id: Int, $model: String) {
  //       addCar2(input: {id: $id, model: $model } ) {
  //         id
  //         model
  //       }
  //     }
  //     `;

  //   this.apollo
  //     .mutate({
  //       mutation: addCar,
  //       variables: {
  //         id: this.id,
  //         model: this.model,
  //         // miles: this.miles
  //       }
  //     })
  //     .subscribe(
  //       ({ data }) => {
  //         console.log("got created data", data);
  //       },
  //       error => {
  //         console.log("there was an error sending the query", error);
  //       }
  //     );
  // }

  updateOrder(order) {
    console.log(this.id, order);
    const updateOrder = gql`
        mutation updatingOrder($id: Int, $client: String, $description: String, $status: String) {
          updateOrder(input: {id: $id, client: $client, description: $description, status: $status } ) {
            id
            client
            description
            status
          }
        }
      `;

    this.apollo
      .mutate({
        mutation: updateOrder,
        variables: {
          id: order.id,
          client: this.client || order.client,
          description: this.description || order.description,
          status: this.status || order.status
        }
      })
      .subscribe(
        ({ data }) => {
          console.log("got edited order", data);
        },
        error => {
          console.log("there was an error sending the query", error);
        }
      );
  }

  addMiles(car) {
    // const updateCar = gql`
    //     mutation updateCar(
    //       $make: String!
    //       $model: String!
    //       $miles: Integer!
    //     ) {
    //       updateCar(
    //         make: $make
    //         model: $model
    //         miles: $miles
    //       ) {
    //         make
    //         model
    //         miles
    //       }
    //     }
    //   `;
    // this.apollo
    //   .mutate({
    //     mutation: updateCar,
    //     variables: {
    //       make: this.selectedRow + 1,
    //       firstName: this.regModel.firstName,
    //       lastName: this.regModel.lastName,
    //       dob: new Date(dateVal),
    //       email: this.regModel.email,
    //       password: this.regModel.password,
    //       country: this.regModel.country
    //     }
    //   })
    //   .subscribe(
    //     ({ data }) => {
    //       console.log("got editdata", data);
    //       this.displayRegistrations();
    //     },
    //     error => {
    //       console.log("there was an error sending the query", error);
    //     }
    //   );
  }

  filterStatus() {
    this.todoQuery = this.apollo.watchQuery({
      query: getOrderByStatusQuery,
      variables: {
        query: `status = '${this.status}'`
      }
    });

    this.ordersSubscription = this.todoQuery.valueChanges.subscribe(
      ({ data }) => {
        console.log('filtered by status', data);
        this.orders = data.orders;
        // this.todoItems = [...data.allTodos];
      }
    );
  }

  ngOnDestroy() {
    this.ordersSubscription.unsubscribe();
  }
}
